# :wave: Hey!

*[Musikee](https://musikee.com/)* is the product I worked on during the last couple of years. It's a tool for the music industry to help artists to engage their fans with activities in exchange for rewards.

The whole development was taken care of by my co-founder and me. We splitted the work between the frontend and the backend. He did the former with [AngularJS](https://angularjs.org/) and I did the latter with [Rails](http://rubyonrails.org/).

It's currently running on a small VPS I configured with [nginx](https://nginx.org/) and [Passenger](https://www.phusionpassenger.com/), and deployed with [Capistrano](http://capistranorb.com/).
