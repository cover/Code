module ProofTypes
  class FacebookShare < Base
    include Twitter::Extractor

    def errors
      @errors = []
      if facebook_response.blank? || facebook_response['type'] != 'og.shares'
        @errors << (proof.required_proof['public'] ? I18n.t('errors.messages.proof_not_public') : I18n.t('errors.messages.proof_only_me'))
      else
        if proof.required_proof['mentions_count'].present? && mentions_count < proof.required_proof['mentions_count']
          @errors << I18n.t('errors.messages.proof_missing_mentions_count', values: mentions_count)
        end
        if proof.required_proof['mentions'].present? && missing_mentions.any?
          @errors << I18n.t('errors.messages.proof_missing_mentions', values: missing_mentions.join(', '))
        end
        if proof.required_proof['hashtags'].present? && missing_hashtags.any?
          @errors << I18n.t('errors.messages.proof_missing_hashtags', values: missing_hashtags.join(', '))
        end
      end
      @errors
    end

    def after_validation
      DeleteFacebookResourceJob.perform_later(data['resource_id']) if @errors.present?
    end

    def after_save
      proof.user.associate_uid(:facebook, facebook_response['from']['id'])
    end

    private

    def custom_proof_attributes
      { text: facebook_response['message'],
        original_response: facebook_response,
        extra_info: { facebook_id: facebook_response['id'] } }
    end

    def facebook_response
      @facebook_response ||= begin
        resource_id = data['resource_id'].to_i
        access_token = proof.required_proof['public'] ? Rails.application.secrets.facebook_app_token : data['access_token']
        fetch_response(resource_id, access_token) || {}
      end
    end

    def fetch_response(resource_id, access_token)
      response = Faraday.get "https://graph.facebook.com/v2.3/#{resource_id}?access_token=#{access_token}&pretty=0"
      response.success? ? JSON.parse(response.body) : nil
    end

    def mentions_count
      @mentions_count ||= facebook_response['message_tags'].present? ? facebook_response['message_tags'].values.flatten.select { |tag| tag['type'] == 'user' }.size : 0
    end

    def missing_mentions
      @missing_mentions ||= begin
        mentions = facebook_response['message_tags'].present? ? facebook_response['message_tags'].values.flatten.map { |p| p['name'].downcase } : []
        required_mentions = proof.required_proof['mentions'].map(&:downcase)
        required_mentions - mentions
      end
    end

    def missing_hashtags
      @missing_hashtags ||= begin
        hashtags = extract_hashtags(facebook_response['message']).map(&:downcase)
        required_hashtags = proof.required_proof['hashtags'].map(&:downcase)
        required_hashtags - hashtags
      end
    end
  end
end
