module ProofTypes
  class Base
    attr_accessor :data, :proof

    def initialize(proof)
      self.proof = proof
      self.data = proof.data
    end

    def proof_attributes
      { file_size: nil, content_type: nil, data: nil, text: nil, extra_info: {},
        asset: nil, original_response: nil, remove_asset: true }.merge(custom_proof_attributes)
    end

    def errors
      []
    end

    def after_validation
      # no op
    end

    def after_save
      # no op
    end

    def after_rollback
      # no op
    end

    private

    def custom_proof_attributes
      raise "#custom_proof_attributes is not defined by #{self.class}"
    end
  end
end
