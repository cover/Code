module ProofTypes
  class Url < Base
    URL_REGEXP = /\Ahttps?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)+([\/?].+)?\z/i

    def errors
      errors = []
      if data.blank?
        errors << I18n.t('errors.messages.blank')
      elsif data.size > 512
        errors << I18n.t('errors.messages.too_long', count: 512)
      elsif !data.match(URL_REGEXP)
        errors << I18n.t('errors.messages.invalid')
      end
      errors
    end

    private

    def custom_proof_attributes
      { text: data }
    end
  end
end
