module ProofTypes
  class MusikeeInvite < Base
    def self.invitation_accepted(enlistment)
      return if enlistment.proof_ref.blank?
      task_id, proof_name, user_id = enlistment.proof_ref.split('-')

      if (task = Task.find_by(id: task_id, user_id: user_id, street_team_id: enlistment.street_team_id)) &&
          task.mission && (mission_proof = task.mission.required_proofs.detect { |p| p['name'] == proof_name })
        Notification.create_invitation_accepted(task, enlistment.user)
         if mission_proof && enlistment.street_team_country && (amount = mission_proof['invitation_credits'].to_i) > 0
           enlistment.credits.create(creditable: enlistment.street_team, credit_type: :invitation, amount: amount, street_team_country: enlistment.street_team_country)
         end
      end
    end

    def errors
      errors = []
      proof_ref = "#{proof.task_id}-#{proof.name}-#{proof.task.user_id}"
      invitations_count = Enlistment.where(proof_ref: proof_ref).where("(points_amounts->>'GLOBAL')::INT > 0").count
      if invitations_count < proof.required_proof['invitations_count']
        errors << I18n.t('errors.messages.proof_invitations_not_enough', count: invitations_count)
      end
      errors
    end

    private

    def custom_proof_attributes
      {}
    end
  end
end
