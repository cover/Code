module ProofTypes
  class TwitterFollow < Base
    def errors
      proof.user.social_networks.where(provider: 'twitter').each do |social_network|
        @friendship = fetch_friendship(social_network)
        return [] if @friendship && @friendship.source.following?
      end

      if @invalid_social_networks.present?
        [I18n.t('errors.messages.unauthorized_on_social_network')]
      else
        [I18n.t('errors.messages.not_following_account', username: proof.required_proof['username'])]
      end
    end

    def after_validation
      return unless @friendship
      proof.original_response = @friendship.to_json
    end

    def after_rollback
      @invalid_social_networks.each(&:destroy) if @invalid_social_networks
    end

    private

    def custom_proof_attributes
      {}
    end

    def fetch_friendship(social_network)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = Rails.application.secrets.twitter_api_key
        config.consumer_secret = Rails.application.secrets.twitter_api_secret
        config.access_token = social_network.token
        config.access_token_secret = social_network.secret
      end

      client.friendship(social_network.uid.to_i, proof.required_proof['username'])
    rescue Twitter::Error::Unauthorized
      @invalid_social_networks ||= []
      @invalid_social_networks << social_network
      return nil
    end
  end
end
