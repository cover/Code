module ProofTypes
  class TwitterTweet < Base
    include Twitter::Extractor

    def errors
      errors = []

      proof.user.social_networks.where(provider: 'twitter').each do |social_network|
        fetch_tweets(social_network).each do |tweet|
          tweet_errors = {}
          tweet_errors[:mentions_count] = missing_mentions_count(tweet)
          tweet_errors[:mentions] = missing_mentions(tweet)
          tweet_errors[:hashtags] = missing_hashtags(tweet)
          tweet_errors[:urls] = missing_urls(tweet)
          if tweet_errors.values.all?(&:blank?)
            @tweet = tweet
            return []
          else
            errors << tweet_errors
          end
        end
      end

      if errors.any?
        errors = errors.sort_by { |error| error.values.flatten.size }.first.map do |field, values|
          values = values.is_a?(Array) ? values.join(',') : values.to_s
          I18n.t("errors.messages.proof_missing_#{field}", values: values) if values.present?
        end

        errors.compact
      elsif @invalid_social_networks.present?
        [I18n.t('errors.messages.unauthorized_on_social_network')]
      else
        [I18n.t('errors.messages.cannot_find_tweets')]
      end
    end

    def after_validation
      return unless @tweet
      proof.text = @tweet.text
      proof.original_response = @tweet.to_json
      proof.extra_info = { twitter_id: @tweet.id, twitter_user_id: @tweet.user.id }
    end

    def after_rollback
      @invalid_social_networks.each(&:destroy) if @invalid_social_networks
    end

    private

    def custom_proof_attributes
      {}
    end

    def fetch_tweets(social_network)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = Rails.application.secrets.twitter_api_key
        config.consumer_secret = Rails.application.secrets.twitter_api_secret
        config.access_token = social_network.token
        config.access_token_secret = social_network.secret
      end

      client.user_timeline
    rescue Twitter::Error::Unauthorized
      @invalid_social_networks ||= []
      @invalid_social_networks << social_network
      return []
    end

    def missing_mentions_count(tweet)
      return nil if proof.required_proof['mentions_count'].blank?
      required_mentions = proof.required_proof['mentions'].present? ? proof.required_proof['mentions'].map(&:downcase) : []
      mentions_count = (tweet.user_mentions.map { |user_mention| user_mention.screen_name.downcase } - required_mentions).size
      mentions_count < proof.required_proof['mentions_count'] ? mentions_count : nil
    end

    def missing_mentions(tweet)
      return [] if proof.required_proof['mentions'].blank?
      mentions = tweet.user_mentions.map { |user_mention| user_mention.screen_name.downcase }
      required_mentions = proof.required_proof['mentions'].map(&:downcase)
      required_mentions - mentions
    end

    def missing_hashtags(tweet)
      return [] if proof.required_proof['hashtags'].blank?
      hashtags = tweet.hashtags.map { |hashtag| hashtag.text.downcase }
      required_hashtags = proof.required_proof['hashtags'].map(&:downcase)
      required_hashtags - hashtags
    end

    def missing_urls(tweet)
      return [] if proof.required_proof['urls'].blank?
      urls = tweet.urls.map { |url| url.expanded_url.to_s.downcase.sub(/\A(http(s)?:\/\/)?(www\.)?/, '') }
      required_urls = proof.required_proof['urls'].map { |url| url.downcase.sub(/\A(http(s)?:\/\/)?(www\.)?/, '') }
      required_urls - urls
    end
  end
end
