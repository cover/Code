module ProofTypes
  class Image < Base
    def errors
      errors = []
      if data.blank?
        errors << I18n.t('errors.messages.blank')
      elsif data.size > 10.megabytes
        errors << I18n.t('errors.messages.size_too_big', file_size: '10 MB')
      end
      errors
    end

    private

    def custom_proof_attributes
      { asset: data, remove_asset: false }
    end
  end
end
