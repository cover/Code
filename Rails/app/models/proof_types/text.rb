module ProofTypes
  class Text < Base
    def errors
      errors = []
      if data.blank?
        errors << I18n.t('errors.messages.blank')
      elsif data.size > 102_400
        errors << I18n.t('errors.messages.too_long', count: 102_400)
      end
      errors
    end

    private

    def custom_proof_attributes
      { text: data }
    end
  end
end
