module ProofTypes
  class SevendigitalBuy < Base
    def errors
      errors = []

      social_network = proof.user.social_networks.find_by(provider: 'seven_digital')

      @purchase = fetch_purchase(social_network)
      if @invalid_social_network.present?
        errors << I18n.t('errors.messages.unauthorized_on_social_network')
      elsif @purchase.blank? ||
          (proof.required_proof['album'] && proof.required_proof['track'].blank? &&
              @purchase.locker_tracks.size < proof.required_proof['album']['trackCount'].to_i)
        errors << I18n.t('errors.messages.seven_digital_not_found')
      end

      errors
    end

    def after_validation
      return unless @purchase
      purchase_date = @purchase.locker_tracks.first.purchase_date
      proof.extra_info = { purchase_date: purchase_date }
    end

    def after_rollback
      @invalid_social_networks.each(&:destroy) if @invalid_social_networks
    end

    private

    def custom_proof_attributes
      {}
    end

    def fetch_purchase(social_network)
      return unless social_network
      client = Sevendigital::Client.new oauth_consumer_key: Rails.application.secrets.seven_digital_key,
                                        oauth_consumer_secret: Rails.application.secrets.seven_digital_secret
      access_token = OAuth::AccessToken.new(client.oauth_consumer, social_network.token, social_network.secret)
      user = client.user.login(access_token)
      album_id = proof.required_proof['album'] && proof.required_proof['album']['id']
      track_id = proof.required_proof['track'] && proof.required_proof['track']['id']
      user.locker(releaseId: album_id, trackId: track_id).locker_releases.first
    rescue Sevendigital::SevendigitalError => e
      if e.error_code == 401
        @invalid_social_networks ||= []
        @invalid_social_networks << social_network
      end
      return []
    end
  end
end
