class Proof < ActiveRecord::Base
  PRIVACIES = { 0 => 'private', 1 => 'public' }

  attr_accessor :data, :tmp_asset_file_name

  belongs_to :activity
  belongs_to :mission, required: true
  belongs_to :task, required: true

  validates :name, uniqueness: { scope: :task_id }

  validate :name_is_valid
  validate :data_is_valid_based_on_the_proof_type, if: :proof_type

  before_validation :set_data_from_required_proof,
                    :set_mission_id_from_task
  before_validation :set_attributes_based_on_the_proof_type, if: :proof_type

  after_validation :call_after_validation_based_on_the_proof_type

  after_save :call_after_save_based_on_the_proof_type

  enum proof_type: [:image, :number, :text, :text_field, :url, :facebook_share, :musikee_invite, :twitter, :sevendigital]
  enum action: [:buy, :tweet, :follow]

  delegate :user, to: :task

  mount_uploader :asset, ProofUploader

  def self.privacies
    PRIVACIES.invert.with_indifferent_access
  end

  def privacy
    PRIVACIES[read_attribute(:privacy)]
  end

  def privacy=(value)
    write_attribute(:privacy, self.class.privacies[value] || 0)
  end

  def required_proof
    return @required_proof if defined? @required_proof
    @required_proof = (task && task.required_proofs.detect { |required_proof| required_proof['name'] == name })
  end

  def manual_after_rollback
    specialized_proof_type.after_rollback if new_record? && proof_type
  end

  private

  def set_data_from_required_proof
    if required_proof
      self.proof_type = required_proof['type']
      self.action = required_proof['action']
      self.privacy = required_proof['privacy']
    else
      self.proof_type = nil
    end
  end

  def set_attributes_based_on_the_proof_type
    return if unchanged_record?
    assign_attributes(specialized_proof_type.proof_attributes)
  end

  def set_mission_id_from_task
    self.mission_id = task&.mission_id
  end

  def name_is_valid
    errors.add(:name, I18n.t('errors.messages.invalid')) unless required_proof
  end

  def data_is_valid_based_on_the_proof_type
    return if unchanged_record?
    specialized_proof_type.errors.each do |error_message|
      errors.add(:data, error_message)
    end
  end

  def unchanged_record?
    return @unchanged_record if defined? @unchanged_record
    @unchanged_record = (persisted? && !name_changed? && !proof_type_changed? && data.nil?)
  end

  def call_after_validation_based_on_the_proof_type
    specialized_proof_type.after_validation if proof_type
  end

  def call_after_save_based_on_the_proof_type
    specialized_proof_type.after_save
  end

  def specialized_proof_type
    @specialized_proof_type ||= "proof_types/#{[proof_type, action].join('_')}".camelize.constantize.new(self)
  end
end

# == Schema Information
#
# Table name: proofs
#
#  id                :integer          not null, primary key
#  task_id           :integer          not null
#  asset             :string
#  file_size         :integer
#  content_type      :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string           not null
#  text              :text
#  proof_type        :integer          not null
#  asset_processing  :boolean          default(FALSE), not null
#  original_response :json
#  extra_info        :hstore           default({}), not null
#  activity_id       :integer
#  privacy           :integer          default(0), not null
#  mission_id        :integer          not null
#  action            :integer
#
# Indexes
#
#  index_proofs_on_activity_id          (activity_id)
#  index_proofs_on_mission_id_and_name  (mission_id,name)
#  index_proofs_on_task_id              (task_id)
#  index_proofs_on_task_id_and_name     (task_id,name) UNIQUE
#
