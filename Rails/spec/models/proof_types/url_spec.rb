require 'rails_helper'

describe ProofTypes::Url, type: :model do
  subject(:url) { ProofTypes::Url.new(double('proof', data: 'https://musikee.com')) }

  describe '#proof_attributes' do
    it 'returns the data as text' do
      expect(url.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil, text: 'https://musikee.com',
                                           extra_info: {}, asset: nil, original_response: nil, remove_asset: true })
    end
  end

  describe '#errors' do
    it 'validates that the url is present' do
      url.data = ''
      expect(url.errors).to eq %w(blank)
    end

    it 'validates that the url is not longer than 512 characters' do
      url.data = "http://domain.com/#{'x' * 495}"
      expect(url.errors).to eq %w(too_long_other|512)

      url.data = "http://domain.com/#{'x' * 494}"
      expect(url.errors).to eq []
    end

    it 'validates that the url is valid' do
      %w(htt://domain.com ftp://domain.com domain.com https://domain).each do |invalid_url|
        url.data = invalid_url
        expect(url.errors).to eq %w(invalid)
      end

      %w(http://domain.com https://domain.com http://sub.domain.com/path1/path2?query=123#/path1/path2?query=321).each do |valid_url|
        url.data = valid_url
        expect(url.errors).to eq []
      end
    end
  end
end
