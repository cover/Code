require 'rails_helper'

describe ProofTypes::FacebookShare, type: :model do
  subject(:facebook_share) { ProofTypes::FacebookShare.new(double('proof', data: { 'access_token' => 'abcde', 'resource_id' => '12345' }, required_proof: {})) }

  describe '#proof_attributes' do
    context 'with valid data' do
      it 'returns the data as text and the original_response' do
        expected_response = { 'id' => '1427032377589603',
                              'from' => { 'id' => '1377328345893340',
                                          'name' => 'Dave Amhdhefbfafb McDonaldman' },
                              'start_time' => '2015-02-20T14:09:09+0000',
                              'publish_time' => '2015-02-20T14:09:09+0000',
                              'application' => { 'name' => 'Musikee - local',
                                                 'namespace' => 'musikeelocal',
                                                 'id' => '1450715021839485' },
                              'message' => 'Test message Karen Amijagfgebhb Yangson Musikee #hashtag',
                              'message_tags' => { '13' => [{ 'id' => '1376760949301148',
                                                             'name' => 'Karen Amijagfgebhb Yangson',
                                                             'type' => 'user',
                                                             'offset' => 13,
                                                             'length' => 26 }],
                                                  '40' => [{ 'id' => '1491856591062134',
                                                             'name' => 'Musikee',
                                                             'type' => 'page',
                                                             'offset' => 40,
                                                             'length' => 7 }] },
                              'data' => { 'object' => { 'id' => '584380631688210',
                                                        'url' => 'https://musikee.com/',
                                                        'type' => 'website',
                                                        'title' => 'Musikee' } },
                              'type' => 'og.shares',
                              'likes' => { 'count' => 0,
                                           'can_like' => true,
                                           'user_likes' => false },
                              'comments' => { 'count' => 0,
                                              'can_comment' => true,
                                              'comment_order' => 'chronological' } }

        VCR.use_cassette('proof-facebook-share-valid') do
          expect(facebook_share.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil,
                                                          text: 'Test message Karen Amijagfgebhb Yangson Musikee #hashtag',
                                                          extra_info: { facebook_id: '1427032377589603' }, asset: nil,
                                                          original_response: expected_response, remove_asset: true })
        end
      end
    end

    context 'with invalid data' do
      it 'returns nil for the text and the original_response' do
        VCR.use_cassette('proof-facebook-share-expired') do
          expect(facebook_share.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil, text: nil,
                                                          extra_info: { facebook_id: nil }, asset: nil, original_response: {}, remove_asset: true })
        end
      end
    end
  end

  describe '#errors' do
    it 'validates that is valid' do
      VCR.use_cassette('proof-facebook-share-valid') do
        facebook_share.proof = double('proof', required_proof: { 'hashtags' => %w(HashTag), 'mentions' => %w(MUSIKEE), 'mentions_count' => 1 })
        expect(facebook_share.errors).to eq []
      end
    end

    it 'validates that the response is valid' do
      VCR.use_cassette('proof-facebook-share-expired') do
        expect(facebook_share.errors).to eq %w(proof_only_me)
      end
    end

    it 'validates that the response type is og.shares' do
      VCR.use_cassette('proof-facebook-share-invalid') do
        expect(facebook_share.errors).to eq %w(proof_only_me)
      end
    end

    it 'validates that the response is public' do
      facebook_share.proof = double('proof', required_proof: { 'public' => true })

      VCR.use_cassette('proof-facebook-share-not-public') do
        expect(facebook_share.errors).to eq %w(proof_not_public)
      end
    end

    it 'validates that the message has the requested number of mentions' do
      facebook_share.proof = double('proof', required_proof: { 'mentions_count' => 2 })

      VCR.use_cassette('proof-facebook-share-valid') do
        expect(facebook_share.errors).to eq %w(proof_missing_mentions_count|1)
      end
    end

    it 'validates that the message has the requested mentions' do
      facebook_share.proof = double('proof', required_proof: { 'mentions' => ['Musikee', 'Van Canto', 'Linkin Park'] })

      VCR.use_cassette('proof-facebook-share-valid') do
        expect(facebook_share.errors).to eq ['proof_missing_mentions|van canto, linkin park']
      end
    end

    it 'validates that the message has the requested hashtags' do
      facebook_share.proof = double('proof', required_proof: { 'hashtags' => %w(Hashtag Musikee 2000) })

      VCR.use_cassette('proof-facebook-share-valid') do
        expect(facebook_share.errors).to eq ['proof_missing_hashtags|musikee, 2000']
      end
    end
  end

  describe '#after_validation' do
    context 'when there are errors' do
      it 'removes the post on Facebook' do
        VCR.use_cassette('proof-facebook-share-invalid') do
          facebook_share.errors
        end

        expect(DeleteFacebookResourceJob).to receive(:perform_later).with('12345')
        facebook_share.after_validation
      end
    end

    context 'when there are no errors' do
      it 'does not remove the post on Facebook' do
        VCR.use_cassette('proof-facebook-share-valid') do
          facebook_share.errors
        end

        expect(DeleteFacebookResourceJob).to_not receive(:perform_later)
        facebook_share.after_validation
      end
    end
  end
end
