require 'rails_helper'

describe ProofTypes::Image, type: :model do
  subject(:image) { ProofTypes::Image.new(double('proof', data: 'random-image-bytes')) }

  describe '#proof_attributes' do
    it 'returns the data as asset' do
      expect(image.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil, text: nil,
                                             extra_info: {}, asset: 'random-image-bytes', original_response: nil, remove_asset: false })
    end
  end

  describe '#errors' do
    it 'validates that the asset is present' do
      image.data = ''
      expect(image.errors).to eq %w(blank)
    end

    it 'validates that the text is not longer than 10 megabytes' do
      image.data = 'x' * 10.megabytes + 'x'
      expect(image.errors).to eq ['size_too_big|10 MB']

      image.data = 'x' * 10.megabytes
      expect(image.errors).to eq []
    end
  end
end
