require 'rails_helper'

describe ProofTypes::Text, type: :model do
  subject(:text) { ProofTypes::Text.new(double('proof', data: 'Musikee is a platform where music artists and labels can coordinate and activate their fans')) }

  describe '#proof_attributes' do
    it 'returns the data as text' do
      expect(text.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil,
                                                 text: 'Musikee is a platform where music artists and labels can coordinate and activate their fans',
                                                 extra_info: {}, asset: nil, original_response: nil, remove_asset: true })
    end
  end

  describe '#errors' do
    it 'validates that the text is present' do
      text.data = ''
      expect(text.errors).to eq %w(blank)
    end

    it 'validates that the text is not longer than 102_400 characters' do
      text.data = 'x' * 102_401
      expect(text.errors).to eq %w(too_long_other|102400)

      text.data = 'x' * 102_400
      expect(text.errors).to eq []
    end
  end
end
