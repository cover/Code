require 'rails_helper'

describe ProofTypes::MusikeeInvite, type: :model do
  subject(:musikee_invite) { ProofTypes::MusikeeInvite.new(proofs(:travel_back_in_time_1_1)) }

  describe '#proof_attributes' do
    it 'does not set any data' do
      expect(musikee_invite.proof_attributes).to eq({ file_size: nil, content_type: nil, data: nil, text: nil,
                                                      extra_info: {}, asset: nil, original_response: nil, remove_asset: true })
    end
  end

  describe '#errors' do
    before do
      enlistments(:stefanie_kuhn_van_canto).update_column :proof_ref, '519544514-i9403012791335705-425868780'
    end

    it 'validates that is valid' do
      expect(musikee_invite.proof).to receive(:required_proof).and_return({ 'invitations_count' => 1 })
      expect(musikee_invite.errors).to eq []
    end

    it 'allows more active enlistments than the required by the mission' do
      enlistments(:jayce_van_canto).update_column :proof_ref, '519544514-i9403012791335705-425868780'
      expect(musikee_invite.proof).to receive(:required_proof).and_return({ 'invitations_count' => 1 })
      expect(musikee_invite.errors).to eq []
    end

    it 'validates that the enlistments are active' do
      enlistments(:stefanie_kuhn_van_canto).update_column :points_amounts, { GLOBAL: 0 }
      expect(musikee_invite.proof).to receive(:required_proof).and_return({ 'invitations_count' => 1 })
      expect(musikee_invite.errors).to eq %w(proof_invitations_not_enough|0)
    end

    it 'validates that the number of active enlistments are enough' do
      expect(musikee_invite.proof).to receive(:required_proof).and_return({ 'invitations_count' => 2 })
      expect(musikee_invite.errors).to eq %w(proof_invitations_not_enough|1)
    end
  end
end
