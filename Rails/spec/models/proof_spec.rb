require 'rails_helper'

describe Proof, type: :model do
  subject(:proof) { proofs(:travel_back_in_time_1_2) }

  it { is_expected.to validate_presence_of :task }

  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:task_id) }

  it { is_expected.to define_enum_for(:proof_type).with([:image, :number, :text, :text_field, :url, :facebook_share, :musikee_invite, :twitter, :sevendigital]) }

  it 'default sorts results by id ASC' do
    expect(Proof.all).to eq [proofs(:travel_back_in_time_1_2),
                             proofs(:travel_back_in_time_2_1),
                             proofs(:travel_back_in_time_1_1)]
  end

  describe 'name' do
    it "is not valid when there is no mission's required proof with the same name" do
      proof.name = 'invalid'
      proof.valid?
      expect(proof.errors[:name]).to include('invalid')
    end
  end

  it 'validates the data based on the proof_type' do
    proof.data = ''
    proof.valid?
    expect(proof.errors[:data]).to include('blank')
  end

  context 'before validation' do
    context 'with a valid name' do
      it "sets the proof_type from mission's required proofs" do
        proof.proof_type = nil
        proof.name = 'i1403012790594065'
        proof.valid?
        expect(proof.proof_type).to eq 'text'
      end

      it 'sets the correct attributes from the data' do
        proof.text = nil
        proof.original_response = { response: 200 }
        proof.asset = 'image.jpg'
        proof.data = '123'
        proof.valid?
        expect(proof.text).to eq '123'
        expect(proof.asset).to be_blank
        expect(proof.original_response).to be_blank
      end
    end

    context 'with an invalid name' do
      it 'sets the proof_type to nil' do
        proof.proof_type = nil
        proof.name = 'invalid'
        proof.valid?
        expect(proof.proof_type).to be_nil
      end
    end
  end

  context 'after validation' do
    it 'calls the custom after validation based on the proof_type' do
      expect_any_instance_of(ProofTypes::Text).to receive(:after_validation)
      proof.save
    end
  end

  context 'after save' do
    it 'calls the custom after save based on the proof_type' do
      expect_any_instance_of(ProofTypes::Text).to receive(:after_save)
      proof.save
    end
  end

  describe '#required_proof' do
    it 'returns the proof required by the mission' do
      mission = double('mission', required_proofs: [{ 'name' => '1234567890', 'type' => 'text' }, { 'name' => '0987654321', 'type' => 'url' }])
      allow(proof.task).to receive(:mission).and_return(mission)
      proof.name = '1234567890'
      expect(proof.required_proof).to eq({ 'name' => '1234567890', 'type' => 'text' })
    end
  end
end

# == Schema Information
#
# Table name: proofs
#
#  id                :integer          not null, primary key
#  task_id           :integer          not null
#  asset             :string
#  file_size         :integer
#  content_type      :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string           not null
#  text              :text
#  proof_type        :integer          not null
#  asset_processing  :boolean          default(FALSE), not null
#  original_response :json
#  extra_info        :hstore           default({}), not null
#  activity_id       :integer
#  privacy           :integer          default(0), not null
#  mission_id        :integer          not null
#  action            :integer
#
# Indexes
#
#  index_proofs_on_activity_id          (activity_id)
#  index_proofs_on_mission_id_and_name  (mission_id,name)
#  index_proofs_on_task_id              (task_id)
#  index_proofs_on_task_id_and_name     (task_id,name) UNIQUE
#
