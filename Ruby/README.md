# :wave: Hello!

I found this [exercise](https://github.com/jhubert/hiring-exercises/tree/master/grouping) a couple of months ago in a job post while browsing [We Work Remotely](https://weworkremotely.com).

I never sent my application for that position, but at that time I found this problem interesting and I solved it just for fun.

As you've probably guessed, the key file in this repository is [group_records](./group_records) :wink:

# Grouping

The goal of this program is to identify rows in a CSV file that
__may__ represent the __same person__ based on a provided __Matching Type__ (definition below).

The resulting program should allow us to test at least three matching types:
 - one that matches records with the same email address
 - one that matches records with the same phone phone number
 - one that matches records with the same email address OR the same phone number

## Resources

### CSV Files

Three sample input files are included. All files should be successfully
processed by the resulting code.

### Matching Type

A matching type is a declaration of what logic should be used to compare the rows.

For example: A matching type named same_email might make use of an algorithm that
matches rows based on email columns.

## Interface

At a high level, the program should take two parameters. The input file
and the matching type.

## Output

The expected output is a copy of the original CSV file with the unique
identifier of the person each row represents prepended to the row.
