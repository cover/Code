# :wave: Greetings!

*Fifty* is my current side project that I'm working on in my spare time.
It's a simple game where you sum the same numbers until they reach at least 50 and they disappear from the board.

I started it because I wanted a small project to try out [React](https://facebook.github.io/react/) and [React Native](https://facebook.github.io/react-native/).

I haven't published it online yet, but I plan to do it soon.
I also want to use this project to learn some marketing basics related to mobile apps.
