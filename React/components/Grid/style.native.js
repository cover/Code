import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  grid: {
    alignSelf: 'stretch',
    backgroundColor: '#91aa9d',
    marginVertical: 5,
    padding: 7
  }
})
