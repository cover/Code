import React, {PropTypes} from 'react'

import GameOver from './components/GameOver/'
import Renderer from './renderer'
import Row from './components/Row/'

const propTypes = {
  generatorIndex: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  onCellClick: PropTypes.func.isRequired,
  onRetry: PropTypes.func.isRequired,
  rows: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
  score: PropTypes.number.isRequired,
  sumsCount: PropTypes.number.isRequired
}

function Grid({
  generatorIndex,
  level,
  onCellClick,
  onRetry,
  rows,
  score,
  sumsCount
}) {
  const mappedRows = rows.map((values, rowIndex) => {
    const rowGeneratorIndex = rowIndex === generatorIndex
      ? generatorIndex
      : null
    return <Row
      key={rowIndex}
      generatorIndex={rowGeneratorIndex}
      onCellClick={onCellClick}
      rowIndex={rowIndex}
      values={values.slice()}/>
  })

  return (
    <Renderer>
      {mappedRows}
      <GameOver
        generatorIndex={generatorIndex}
        level={level}
        onRetry={onRetry}
        rows={rows}
        score={score}
        sumsCount={sumsCount}/>
    </Renderer>
  )
}

Grid.propTypes = propTypes

export default Grid
