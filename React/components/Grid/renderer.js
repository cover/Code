import React, {PropTypes} from 'react'

import './style.css'

const propTypes = {
  children: PropTypes.array.isRequired
}

function Renderer({children}) {
  return (
    <div className="grid">
      {children}
    </div>
  )
}

Renderer.propTypes = propTypes

export default Renderer
