import React, {PropTypes} from 'react'
import {View} from 'react-native'

import style from './style'

const propTypes = {
  children: PropTypes.array.isRequired
}

function Renderer({children}) {
  return (
    <View style={style.grid}>
      {children}
    </View>
  )
}

Renderer.propTypes = propTypes

export default Renderer
