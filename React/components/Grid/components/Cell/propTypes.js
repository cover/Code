import {PropTypes} from 'react'

export default {
  isGenerator : PropTypes.bool.isRequired,
  onClick : PropTypes.func.isRequired,
  value : PropTypes.number
}
