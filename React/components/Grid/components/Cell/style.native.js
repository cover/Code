import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  cell: {
    backgroundColor: '#eef9d7',
    borderRadius: 10,
    flex: 1,
    margin: 7,
    paddingVertical: 10
  },

  cellText: {
    color: '#193441',
    fontFamily: 'VarelaRound-Regular',
    fontSize: 25,
    textAlign: 'center'
  },

  generator: {
    backgroundColor: '#193441',
    color: 'white'
  }
})
