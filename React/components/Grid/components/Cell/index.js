import React, {Component} from 'react'

import './style.css'

import {fadeIn, fadeOut} from 'services/Fade/'
import propTypes from './propTypes'

class Cell extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.isGenerator || nextProps.value !== this.props.value
  }

  componentWillUpdate(nextProps) {
    fadeOut(this.domElement)
  }

  componentDidUpdate(prevProps) {
    fadeIn(this.domElement)
  }

  domRef = ref => this.domElement = ref

  render() {
    const {isGenerator, onClick, value} = this.props
    if (isGenerator) {
      return (
        <div className="grid-generator" ref={this.domRef}>{value.toLocaleString()}</div>
      )
    } else {
      return (
        <div className="grid-cell" ref={this.domRef} onClick={onClick}>{value && value.toLocaleString()}</div>
      )
    }
  }
}

Cell.propTypes = propTypes

export default Cell
