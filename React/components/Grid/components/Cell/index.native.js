import React, {Component} from 'react'
import {Text, TouchableHighlight} from 'react-native'

import style from './style'

import propTypes from './propTypes'

class Cell extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.isGenerator || nextProps.value !== this.props.value
  }

  render() {
    const {isGenerator, onClick, value} = this.props
    if (isGenerator) {
      return (
        <Text style={[style.cell, style.cellText, style.generator]}>{value.toLocaleString()}</Text>
      )
    } else {
      return (
        <TouchableHighlight
          style={style.cell}
          onPress={onClick}
          underlayColor="#fffcd8">
          <Text style={style.cellText}>{(value && value.toLocaleString()) || ' '}</Text>
        </TouchableHighlight>
      )
    }
  }
}

Cell.propTypes = propTypes

export default Cell
