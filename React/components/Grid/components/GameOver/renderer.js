import React, {PropTypes} from 'react'

import './style.css'

import Button from 'components/Button/'
import MailChimpForm from 'components/MailChimpForm/'
import TweetButton from 'components/TweetButton/'

const propTypes = {
  hasGameEnded: PropTypes.bool,
  onRetry: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  twitterText: PropTypes.string.isRequired
}

function Renderer({hasGameEnded, onRetry, score, text, twitterText}) {
  // FIXME temporary fix for the z-index bug:
  // https://github.com/facebook/react-native/pull/10954
  if (!hasGameEnded) {
    return null
  }

  return (
    <div className="grid-game-over">
      <h2>{text}</h2>
      <p>Your score: {score.toLocaleString()}</p>
      <p>
        <Button onClick={onRetry}>Try Again</Button>
        <span>or</span>
        <TweetButton text={twitterText}/>
      </p>
      <MailChimpForm/>
    </div>
  )
}

Renderer.propTypes = propTypes

export default Renderer
