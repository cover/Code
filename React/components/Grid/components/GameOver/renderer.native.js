import React, {PropTypes} from 'react'
import {Text, View} from 'react-native'

import style from './style'

import Button from 'components/Button/'
import MailChimpForm from 'components/MailChimpForm/'
import TweetButton from 'components/TweetButton/'

const propTypes = {
  hasGameEnded: PropTypes.bool,
  onRetry: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  twitterText: PropTypes.string.isRequired
}

function Renderer({hasGameEnded, onRetry, score, text, twitterText}) {
  // FIXME temporary fix for the z-index bug:
  // https://github.com/facebook/react-native/pull/10954
  if (!hasGameEnded) {
    return (<View style={style.zIndexWorkaround}/>)
  }

  return (
    <View style={style.container}>
      <Text style={[style.text, style.header]}>{text}</Text>
      <Text style={style.text}>Your score: {score.toLocaleString()}</Text>
      <View style={style.buttons}>
        <Button onClick={onRetry}>Try Again</Button>
        <Text style={[style.text, style.buttonsText]}>or</Text>
        <TweetButton text={twitterText}/>
      </View>
      <MailChimpForm/>
    </View>
  )
}

Renderer.propTypes = propTypes

export default Renderer
