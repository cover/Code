import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  buttons: {
    flexDirection: 'row',
    marginTop: 25
  },

  buttonsText: {
    marginHorizontal: 15
  },

  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    bottom: 0,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },

  header: {
    fontSize: 35,
    fontWeight: 'bold'
  },

  text: {
    color: 'white',
    fontSize: 25
  },

  zIndexWorkaround: {
    height: 0
  }
})
