import React, {Component, PropTypes} from 'react'

import AppStorage from 'services/AppStorage/'
import GoogleAnalytics from 'services/GoogleAnalytics/'
import Renderer from './renderer'

const propTypes = {
  generatorIndex: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  onRetry: PropTypes.func.isRequired,
  rows: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
  score: PropTypes.number.isRequired,
  sumsCount: PropTypes.number.isRequired
}

class GameOver extends Component {
  componentWillUpdate(nextProps) {
    const {generatorIndex, level, rows} = nextProps
    const generatorValue = rows[generatorIndex][generatorIndex]
    let availableCells = 0
    rows.forEach((row, rowIndex) => {
      row.forEach((value, columnIndex) => {
        if (value === null || value === generatorValue) {
          availableCells++
        }
      })
    })
    const totalCells = rows.length * rows.length
    this.hasWon = generatorValue >= Math.ceil(level / 2) && availableCells === totalCells
    this.hasGameEnded = availableCells <= 1 || this.hasWon
  }

  componentDidUpdate() {
    if (this.hasGameEnded) {
      const {level, score, sumsCount} = this.props
      const action = this.hasWon
        ? 'won'
        : 'lost'
      if (this.hasWon) {
        AppStorage.setItem('level', level + 1)
      }
      GoogleAnalytics.trackEvent({action, level, score, sumsCount})
    }
  }

  render() {
    const {onRetry, score} = this.props
    const result = this.hasWon
      ? {
        text: 'You Won!',
        twitterText: `I finally won at summing numbers at Fifty with a score of ${score} – Try to beat me!`
      }
      : {
        text: 'Game Over!',
        twitterText: `I've achieved a score of ${score} by summing numbers at Fifty – Try to beat me!`
      }
    return (<Renderer
      hasGameEnded={this.hasGameEnded}
      onRetry={onRetry}
      score={score}
      text={result.text}
      twitterText={result.twitterText}/>)
  }
}

GameOver.propTypes = propTypes

export default GameOver
