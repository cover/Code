import React, {PropTypes} from 'react'

const propTypes = {
  children: PropTypes.array.isRequired
}

function Renderer({children}) {
  return (
    <div>
      {children}
    </div>
  )
}

Renderer.propTypes = propTypes

export default Renderer
