import React, {Component, PropTypes} from 'react'

import Cell from '../Cell/'
import Renderer from './renderer'

const propTypes = {
  generatorIndex: PropTypes.number,
  onCellClick: PropTypes.func.isRequired,
  rowIndex: PropTypes.number.isRequired,
  values: PropTypes.arrayOf(PropTypes.number).isRequired
}

class Row extends Component {
  shouldComponentUpdate(nextProps) {
    return !!nextProps.generatorIndex || nextProps.values.join() !== this.props.values.join()
  }

  render() {
    const {generatorIndex, onCellClick, rowIndex, values} = this.props
    const cells = values.map((value, cellIndex) => {
      return <Cell
        key={cellIndex}
        isGenerator={cellIndex === generatorIndex}
        onClick={() => onCellClick(rowIndex, cellIndex)}
        value={value}/>
    })

    return (
      <Renderer>
        {cells}
      </Renderer>
    )
  }
}

Row.propTypes = propTypes

export default Row
